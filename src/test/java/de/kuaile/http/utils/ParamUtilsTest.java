/**
 * 
 */
package de.kuaile.http.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import de.kuaile.httputils.ParamUtils;

/**
 *
 * @author Csen
 *
 */
public class ParamUtilsTest {

	@Test
	public void decoder(){
		try {
			String encode = URLEncoder.encode("中华人民共和国","utf-8");
			System.out.println(encode);
			Assert.assertEquals(URLDecoder.decode(encode,"utf-8"), ParamUtils.decoder(encode));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void encode(){
		try {
			String encode =  ParamUtils.encode("中华人民共和国");
			Assert.assertEquals(URLEncoder.encode("中华人民共和国","utf-8"),encode);
			System.out.println(encode);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void queryToMap(){
		String queryString = "name=zhangsan&age=24&sex=0";
		Map<String, String> queryMap =  ParamUtils.queryToMap(queryString);
		Assert.assertEquals(3,queryMap.size());
		System.out.println(queryMap);
	}
	@Test
	public void mapToQuery(){
		String queryString = "name=zhangsan&age=24&sex=0";
		Map<String, String> queryMap =  ParamUtils.queryToMap(queryString);
		queryMap.put("t", "wxm2-login");
		queryMap.put("lang", "zh_CN");
		queryMap.put("ajax", "true");
		String query = ParamUtils.mapToQuery(queryMap);
		//Assert.assertEquals(query, queryString);
		System.out.println(query);
	}
	@Test
	public void notEmpty(){
		Assert.assertEquals(true, ParamUtils.notEmpty("aaaa","bbbb"));
	}
}
